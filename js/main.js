'use strict'

/**
 * Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.
 *
 * AJAX - це методологія побудови веб-сторінки, яка передбачає отримання даних з серверу
 * та відповідне їх відображення на сторінці без її оновлення.
 * AJAX дозволяє побудувати архітектуру додатку як систему html-шаблнів, які заповнюються даними
 * в залежності від активності (запитів) користувача (наприклад, в інтернет-магазині є один шаблон
 * сторінки товару який заповнюється даними того чи іншого товару із БД в залежності від вибору
 * користувача). Також, при побудові сторінки з великою кількістю даних що можуть довго завантажуватися,
 * AJAX дозволяє при завантаженні сторінки надати користувачу готовий (або швидко завантажуваний контент)
 * ще до завантаження основних даних з серверу.
 * */

const node = document.querySelector('.star-wars')

fetch('https://ajax.test-danit.com/api/swapi/films')
    .then(response => response.json())
    .then(data => {

        data.forEach(obj => {
            let {name, episodeId, openingCrawl, characters} = obj
            let episode = document.createElement('div');

            episode.innerHTML = `
                <h2>${name}</h2>
                <p>Episode: ${episodeId}</p>
                <p>Description: ${openingCrawl}</p>
                <p>Characters:</p>
            `

            node.append(episode)

            let chars = document.createElement('ol')
            chars.classList.add('chars')
            let loader = document.createElement('div');
            loader.classList.add('loader')
            loader.innerHTML = `
                          <div class="bar bar1"></div>
                          <div class="bar bar2"></div>
                          <div class="bar bar3"></div>
                          <div class="bar bar4"></div>
                          <div class="bar bar5"></div>
                          <div class="bar bar6"></div>
                          <div class="bar bar7"></div>
                          <div class="bar bar8"></div>
                        `
            chars.append(loader)

            episode.append(chars)

            characters.forEach(el => {
                fetch(`${el}`)
                    .then(response => response.json())
                    .then(data => {
                        let listItem = document.createElement('li')
                        listItem.innerHTML = data.name;
                        chars.append(listItem)
                    })
                    .finally(() => { document.querySelector('.loader').remove()} )
            })


        })


    })
    //.finally(() => { document.querySelector('.loader').remove()} )